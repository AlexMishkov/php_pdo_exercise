<?php
require_once 'DB.php';



use SiteCreator\DB;

// if($_SERVER['REQUEST_METHOD' == 'POST']){


// }else{
// header('Location: form.php');
// }

$site = DB::getData([$_GET['id']]);



$bgLink = $site['bgLink'];
$title = $site['title'];
$alternateTitle = $site['alternateTitle'];
$yourself = $site['yourself'];
$location = $site['location'];
$phone = $site['phone'];
$offer = $site['offer'];
$imgProduct1 = $site['imgProduct1'];
$descProduct1 = $site['descProduct1'];
$imgProduct2 = $site['imgProduct2'];
$descProduct2 = $site['descProduct2'];
$imgProduct3 = $site['imgProduct3'];
$descProduct3 = $site['descProduct3'];
$descCompany = $site['descCompany'];
$linkedin = $site['linkedin'];
$facebook = $site['facebook'];
$twitter = $site['twitter'];
$google = $site['google'];





?>

<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

    <!-- Fontawesome -->
    <script src="https://use.fontawesome.com/563bb0680c.js"></script>

    <!-- Personal CSS -->
    <style>
        html {
            scroll-behavior: smooth;
        }

        body {
            margin: 0;
            padding: 0;
            box-sizing: border-box;

        }

        .bg-color {
            background-color: rgb(255, 215, 143);
        }

        .card-img {
            height: 60%;
        }

        .self-border {
            border: 4px black solid;
        }

        .nav-item a:hover{
            background-color: ghostwhite;
            color: black !important;
        }
    </style>


    <title>Challenge 16</title>
</head>

<body data-spy="scroll" data-target="#navbar-self" class="bg-color">
    <nav id="navbar-self" class="navbar navbar-expand-lg navbar-dark bg-primary">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link rounded" href="#home">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link rounded" href="#about-us">About Us</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link rounded" href="#offer"><?= $offer ?>s</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link rounded" href="#contact">Contact</a>
                </li>

                </form>
        </div>
    </nav>

    <div class="container-fluid">
        <div class="row" id="home">
            <div class="col-md-12 px-0" style="height: 50vh;">
                <div class="jumbotron-fluid">
                    <img src="<?= $bgLink ?>" alt="" class="w-100 position-absolute" style="height: 50vh;">
                    <div class="container">
                        <h1 class="display-4 text-center pt-5 position-relative text-white"><?= $title ?></h1>
                        <p class="lead text-center pt-5 position-relative text-white"><?= $alternateTitle ?></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row pt-5" id="about-us">
            <div class="col-md-8">
                <h3>About Us</h3>
                <p><?= $yourself ?></p>
            </div>
            <div class="col-md-4 text-center">
                <h6>Phone</h6>
                <p class="pt-2"><?= $phone ?></p>
                <h6>Location</h6>
                <p class="pt-2"><?= $location ?></p>
            </div>
        </div>
        <div class="row pb-5 pb-md-0" id="offer">
            <div class="col-12 text-center mb-4">
                <h2><?= $offer ?>s</h2>
            </div>
            <div class="col-12">
                <div class="card-group">
                    <div class="card bg-color bg-color border-0">
                        <img src="<?= $imgProduct1 ?>" class="card-img-top card-img" alt="...">
                        <div class="card-body overflow-auto">
                            <h5 class="card-title">First <?= $offer ?></h5>
                            <p class="card-text"><?= $descProduct1 ?></p>
                        </div>
                    </div>
                    <div class="card bg-color bg-color ml-sm-4 border-0">
                        <img src="<?= $imgProduct2 ?>" class="card-img-top card-img" alt="...">
                        <div class="card-body overflow-auto">
                            <h5 class="card-title">Second <?= $offer ?></h5>
                            <p class="card-text"><?= $descProduct2 ?></p>
                        </div>
                    </div>
                    <div class="card bg-color bg-color ml-sm-4 border-0">
                        <img src="<?= $imgProduct3 ?>" class="card-img-top card-img" alt="...">
                        <div class="card-body overflow-auto">
                            <h5 class="card-title">Third <?= $offer ?></h5>
                            <p class="card-text over"><?= $descProduct3 ?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mb-5" id="contact">
            <div class="col-12 pb-5">
                <h2 class="text-center">Contact</h2>
            </div>

            <div class="col-md-6 col-12">
                <h3>Company description</h3>
                <p><?= $descCompany ?></p>
            </div>
            <div class="col-md-5 offset-md-1 col-12 mt-sm-0 mt-5">
                <form class="bg-info rounded pt-4 self-border">
                    <div class="form-group px-3">
                        <label for="" class="font-weight-bold">Your name</label>
                        <input type="text" class="form-control" name="name">
                    </div>
                    <div class="form-group px-3">
                        <label for="" class="font-weight-bold">Your email</label>
                        <input type="email" class="form-control" name="email">
                    </div>
                    <div class="form-group px-3">
                        <label for="" class="font-weight-bold">Message</label>
                        <textarea class="form-control" id="" cols="10" rows="5" name="message"></textarea>
                    </div>
                    <div class="text-center pt-1 pb-3">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>
        <div class="row bg-primary">
            <div class="col-sm-9 col-12">
                <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Sapiente ut voluptatum quo reiciendis animi soluta pariatur ut voluptatum quo reiciendis animi soluta pariatur</p>
            </div>
            <div class="col-sm-3 col-12 text-center">
                <a href="<?= $linkedin ?>"><i class="fa fa-linkedin bg-color text-primary p-1 rounded mt-sm-3"></i></a>
                <a href="<?= $facebook ?>"><i class="fa fa-facebook-square bg-color text-primary p-1 rounded mt-sm-3"></i></a>
                <a href="<?= $twitter ?>"><i class="fa fa-twitter-square bg-color text-primary p-1 rounded mt-sm-3"></i></a>
                <a href="<?= $google ?>"><i class="fa fa-google bg-color text-primary p-1 rounded mt-sm-3 mb-3 mb-sm-0"></i></a>
            </div>
        </div>



    </div>






    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script>

</body>

</html>