<?php

namespace SiteCreator;


class Site {
        private $bgLink;
        private $title;
        private $alternateTitle;
        private $yourself;
        private $location;
        private $phone;
        private $offer;
        private $imgProduct1;
        private $descProduct1;
        private $imgProduct2;
        private $descProduct2;
        private $imgProduct3;
        private $descProduct3;
        private $descCompany;
        private $linkedin;
        private $facebook;
        private $twitter;
        private $google;
        private $table = "site_creator";


    public function __construct($bgLink, $title, $alternateTitle, $yourself, $phone, $location, $offer, $imgProduct1, $descProduct1, $imgProduct2, $descProduct2, 
    $imgProduct3, $descProduct3, $descCompany, $linkedin, $facebook, $twitter, $google)
    {
        $this->bgLink = $bgLink;
        $this->title = $title;
        $this->alternateTitle = $alternateTitle;
        $this->yourself = $yourself;
        $this->location = $location;
        $this->phone = $phone;
        $this->offer = $offer;
        $this->imgProduct1 = $imgProduct1;
        $this->descProduct1 = $descProduct1;
        $this->imgProduct2 = $imgProduct2;
        $this->descProduct2 = $descProduct2;
        $this->imgProduct3 = $imgProduct3;
        $this->descProduct3 = $descProduct3;
        $this->descCompany = $descCompany;
        $this->linkedin = $linkedin;
        $this->facebook = $facebook;
        $this->twitter = $twitter;
        $this->google = $google;
        
        
    }

    public function getColumns(){
        return[
            "bgLink",
            "title",
            "alternateTitle",
            "yourself",
            "location",
            "phone",
            "offer",
            "imgProduct1",
            "descProduct1",
            "imgProduct2",
            "descProduct2",
            "imgProduct3",
            "descProduct3",
            "descCompany",
            "linkedin",
            "facebook",
            "twitter",
            "google"
        ];
    }

    public function getRealData(){
        return[
            $this->bgLink,
            $this->title,
            $this->alternateTitle,
            $this->yourself,
            $this->location,
            $this->phone,
            $this->offer,
            $this->imgProduct1,
            $this->descProduct1,
            $this->imgProduct2,
            $this->descProduct2,
            $this->imgProduct3,
            $this->descProduct3,
            $this->descCompany,
            $this->linkedin,
            $this->facebook,
            $this->twitter,
            $this->google
        ];
    }

    public function getTable(){
        return $this->table;
    }
}