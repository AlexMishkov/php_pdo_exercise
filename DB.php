<?php

namespace SiteCreator;
require_once "constants.php";
require_once "Site.php";

use mysqli;
use SiteCreator\Site;
use \PDO;



class DB {

    private static $pdo = null;
    

    public static function connect(){
        
        if(is_null(self::$pdo)){
            try{
                self::$pdo = new \PDO("mysql:dbname=" . DBNAME . ";host=" . HOST, MYACCOUNT, MYPASSWORD);
            }catch(\PDOException $e){
                die("Can not connect");
            }
        }
    }

    public static function insert($object){
        self::connect();
        
        $table = $object->getTable();
        $columns = $object->getColumns();
        $realColumns = implode(",", $columns);
        $forPrepare = [];
        foreach($columns as $column){
            array_push($forPrepare, "?");
        }

        $realForPrepare = implode(",", $forPrepare);

        $sql = "INSERT INTO $table ($realColumns)
                            VALUES($realForPrepare)";
            // echo $sql . "<br>";
            // print_r($object->getRealData());
            // die();

        $stmt = self::$pdo->prepare($sql);
        
        

        if($stmt->execute($object->getRealData())){
            $id = self::$pdo->lastInsertId();
            header("Location: myPage.php?id=" . $id);
        }else {
            echo "not connected";
            
            header("Location: form.html");
            die();
        }
    }

    public static function getData($id){
        self::connect();

        
        $sql = "SELECT bgLink,title,alternateTitle,yourself,`location`,phone,offer, 
        imgProduct1,descProduct1,imgProduct2,descProduct2,imgProduct3,descProduct3,descCompany,
        linkedin,facebook,twitter,google FROM site_creator WHERE id = ?";

        $stmt = self::$pdo->prepare($sql);

        $stmt->execute($id);

        while($row = $stmt->fetch()){
            return $row;
            
            // die();
            // $row['bgLink'] . ", " . $row['title'] . ", " . $row['alternateTitle'] . ", " . $row['yourself'] . ", " . $row['location'] . ", " . $row['phone'] . ", " . $row['offer']
            // . ", " . $row['imgProduct1'] . ", " . $row['descProduct1'] . ", " . $row['imgProduct2'] . ", " . $row['descProduct2'] . ", " . $row['imgProduct3'] . ", " . $row['descProduct3']
            // . ", " . $row['descCompany'] . ", " . $row['linkedin'] . ", " . $row['facebook'] . ", " . $row['twitter'] . ", " . $row['google'];
        }
        

        

    }
}