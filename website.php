<?php


require_once "DB.php";

use SiteCreator\Site;
use SiteCreator\DB;

$bgLink = $_POST["bgLink"];
$title = $_POST["title"];
$alternateTitle = $_POST["alternateTitle"];
$yourself = $_POST["yourself"];
$location = $_POST["location"];
$phone = $_POST["phone"];
$offer = $_POST["offer"];
$imgProduct1 = $_POST["imgProduct1"];
$descProduct1 = $_POST["descProduct1"];
$imgProduct2 = $_POST["imgProduct2"];
$descProduct2 = $_POST["descProduct2"];
$imgProduct3 = $_POST["imgProduct3"];
$descProduct3 = $_POST["descProduct3"];
$descCompany = $_POST["descCompany"];
$linkedin = $_POST["linkedin"];
$facebook = $_POST["facebook"];
$twitter = $_POST["twitter"];
$google = $_POST["google"];


$website = new Site($bgLink, $title, $alternateTitle, $yourself, $location, $phone, $offer, $imgProduct1, $descProduct1, $imgProduct2, 
                    $descProduct2, $imgProduct3, $descProduct3, $descCompany, $linkedin, $facebook, $twitter, $google);

DB::insert($website);


?>
